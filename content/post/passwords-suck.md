+++
title = "Why Passwords Suck"
date = "2023-02-12"
author = "Piper"
description = "Or how we learned to stop worrying and love the WebAuthn API"
tags = ["authentication", "webauthn"]
+++

## Passwords Suck!
Passwords are in use everywhere, and it seems no one can agree on what makes a good password. 

The NIST recommends that passwords should be hard for computers to guess. So they should be over a minimum length that makes this difficult to do. Typically, the minimum is 8-10 characters. 
The NIST also recommends that passwords be easy for humans to remember. Many sites require passwords to include a mixture of capital and lowercase letters, numbers, and special characters. 

> Example password: `4Pg$XYb6kY` 

This is anything but easy for humans to remember. The problem only gets worse if we increase the length of the password for greater security.

For this reason, the NIST now recommends that long passwords be made by concatenating four or five dictionary words, placing numbers or special characters in between. 
> Example NIST password: `triadic5bank%foot4tumble$couch`

This is an improvement, in that its length makes it harder for computers to guess, and the use of dictionary words makes it easier for humans to remember, but we can still do better. 

Because passwords are often difficult for humans to remember, its not unusual for people to reuse them for different logins. This reduces the number of passwords someone needs to remember, but it also means if one password is compromised, your other accounts are also at risk. 

Security conscious folks use a unique, long password for each login they use, and keep them secure and organized in a password manager. If you're doing this now, good for you! You're taking your security seriously, and I don't suppose you'd have read this far if you weren't at least planning to get better about it. 

Ok, so what is wrong with this solution? It seems to cover most bases, we have long passwords, they can be unique, randomly generated, and include all the required mixtures of characters demanded by even the most pedantic of sign up pages. So where's the trouble?

Well, skipping over the obvious problem of where to store your password for your password manager, and what to do if you lose or forget it, the majority of password compromises are not due to a good guess. They are due to phishing. 

## Phishing Sucks!
A phishing attack is where by various means, a user is coaxed into visiting a domain that either pretends to be a familiar and trusted site, or acts as a man in the middle between the user and a familiar and trusted site. In both cases, when the user enters their login credentials, the attacker is able to record the password for later use. 

Other security savvy readers may note that I have failed to mention two factor authentication (2FA). This is because, while 2FA can certainly help in the case where a password is discovered or guessed, they don't provide protection against more sophisticated phishing attacks, and the attacker also has access to record the TOPT supplied by the second factor.

Along with phishing, another common source of account compromises are from data breaches, where entire databases of user logins have been leaked to the dark web for use in fraud. Several services exist to aid users in identifying if their logins have been leaked in such a manner, and some password managers also include this feature. 

If protecting your online  seems like growing burden for you, you're certainly not alone. 

## Enter Webauthn
Webauthn is a new W3C [standard](https://www.w3.org/TR/webauthn-2/) for passwordless authentication. Instead of passwords, it uses a public and private key pair unique for each device a user might register with. The private key stays on the users device, and is used to cryptographically sign a challenge (usually a long random number) supplied by the server. The server will use the users corresponding public key to validate the signed challenge, authenticating the user. 

From a user experience, registration and login typically involves no more than entering a username and using their device's onboard biometric reader, or a pin number. 

Users may also choose to use a third party hardware authenticator, such as a Yubikey or Nitrokey.

No more passwords, no more phishing, no more accounts compromised by data breaches or malicious websites. Just ease of use, and peace of mind. You deserve nothing less. 

