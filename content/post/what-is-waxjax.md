+++
title = "What is WaxJack?"
date = "2023-02-15"
author = "Piper"
description = "What does this thing do, anyway?"
tags = ["waxjack", "identity", "reputation", "blockchain"]
+++

## What is WaxJack?

WaxJack is a collection of online services, browser extensions, and eventually mobile and desktop apps that allows audiences to prove the content they view is from the creators they have grown to trust. 

## Why tho?
In an age where online impersonation, forgeries, and account compromises can have consequences ranging anywhere from embarrassment, lost revenue, to swayed political outcomes, and disruption of important social justice movements, it has become ever more clear that trust between both creators and audience must be protected from malicious actors. 

Recent advances in language models only compounds the problem. How do you even know an online creator is a person at all, let alone the one you believe them to be?

## So How Does It Work?
The basic idea is that each creator has a public and private key pair. The private key is kept safely on the users device, and if a user has more than one device, each has a unique key pair. 

The public key is stored in a database when the user registers. 

On the client side, when a creator wishes to prove authorship of some content, the client will create a unique hash of the content, and then sign that hash with their private key. The content hash and the signature are both hashed to produce a unique URL path. 

A benefit of this particular approach to generating URLs is that the validity of URLs can be verified without issuing a request to them.

The content hash and the signature, but not the content itself, is then sent to the server where it is stored in a database for later retrieval via the URL created in the prior step.

The creator then publishes the content along with the URL. 

When an audience member wishes to verify the authorship of some content they are viewing, they can click the URL and will be brought to a verification page with the hash values and signatures, and the public key used to produce them. 

A browser extension is also in the works that will render some sort of symbol to show content is valid without any user interaction or following any links. 

## Why not put them on a blockchain?

While centralization often raises trust concerns that blockchains claim to resolve, its not without some significant trade-offs. 

For example, blockchains rely on the security of private key encryption to the degree that private data is stored publicly. This is an enormous leap of faith, as it is not historically unusual for flaws to be found in crypto algorithms that are in widespread use. 

Such a scenario would spell instant certain doom for any blockchain that relies on those algorithms. Luckily for us, we don't store anything but public data. Your private keys and other private data stays safely on your device.

A more likely problem to encounter is that of permanence. Because entries cannot be removed from a blockchain, any proof of authorship is permanent. Bad ideas you had a decade ago are forever a part of the blockchain, threatening to rise up from the dead at any moment of your life and cause you a lot trouble you don't deserve or need. 

We've all heard a teacher threaten to put some misbehavior on a student's "permanent record." To this day I wonder if such a thing even exists. The blockchain makes it very real. This just feels deeply out of line with the long history of human societies, and quite frankly rather dystopian. We think you should be able to delete stuff. 

From a technical standpoint, blockchains are just plain slow.. 

Bitcoin can only guarantee 4.6 transactions per second. Ethereum is significantly better at about 30 transactions per second. But a service of this kind absolutely relies on being able to deliver positive verification of each piece of content to potentially millions of audience members. And it has to do it fast, and it has to be easy to use, or people just won't use it. 

...and a nightmare to scale. Every device on the blockchain network has to store a copy of the chain, this could mean potentially millions of copies. It requires massive storage, and it only gets worse with time. 

That brings us to the final nail in the coffin for blockchain in this application, the incredible waste of resources making every node verify every transaction on its own copy of the chain. As people who take seriously our place on planet Earth, conserving the planets vital resources is not optional.

While we are a centralized service, we think you'll come to appreciate this choice once you get a feel for the service. 

As we don't store any private information, there is nothing useful to hackers to breach. As all authorship proofs can be verified with existing tools like openssl, you don't have to trust us to tell you what is authentic or not, even if we cease to exist. 
But we sincerely hope we'll earn your trust, and in the process, help foster greater trust between real people online.

